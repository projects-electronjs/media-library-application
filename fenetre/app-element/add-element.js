const remote = require('electron').remote;

// ===== Evenement sur le lien closeWindow =====
document.querySelector(`#close-window`).addEventListener('click', () => window.close())

// ===== Fonctions de récuperation de donnée du formulaire =====
document.querySelector('#button-validation').addEventListener('click', () => {
    
    let newElements = {
        valueTitre: document.querySelector(`#titre`).value,
        valueFile: document.querySelector(`#file`).value.replace(/^.*\\/, ""),
    }

    let arrayElements = [];

    if(localStorage.getItem('elements') !== null){
        arrayElements = JSON.parse(localStorage.getItem('elements'))
    }

    if(newElements.valueFile !== "" && newElements.valueTitre !== ""){
        arrayElements.push(newElements);
        localStorage.setItem('elements', JSON.stringify(arrayElements));
    }

    remote.getCurrentWindow().getParentWindow().send('element-added')
    
})
